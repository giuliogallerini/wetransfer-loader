module.exports = {
    parser: "@typescript-eslint/parser",

    plugins: ["@typescript-eslint"],

    extends: [
        "airbnb-typescript",
        "plugin:@typescript-eslint/recommended",
        "plugin:jest/recommended",
        "plugin:import/typescript",
    ],
    rules: {
        "@typescript-eslint/no-unused-vars": "error",
        "@typescript-eslint/no-unused-expressions": [
            "error",
            { allowTernary: true },
        ],
        "no-underscore-dangle": [
            "error",
            {
                allow: [
                    "__INITIAL_STATE__",
                    "__REDUX_DEVTOOLS_EXTENSION_COMPOSE__",
                ],
            },
        ],
        "implicit-arrow-linebreak": "off",
        "no-confusing-arrow": "off",
        "no-named-as-default-member": "off",
        "import/no-named-as-default": "off",
        "react/jsx-props-no-spreading": [
            0,
            { html: "ignore", custom: "ignore" },
        ],
        "import/prefer-default-export": "off",
        "react/state-in-constructor": "off",
        "react/static-property-placement": "off",
        "@typescript-eslint/naming-convention": [
            "error",
            {
                selector: "enumMember",
                format: ["camelCase"],
            },
        ],
        "react/react-in-jsx-scope": 0,
    },

    env: {
        jest: true,
        browser: true,
    },

    settings: {
        "import/resolver": {
            node: {
                paths: ["src"],
                extensions: [".js", ".jsx", ".ts", ".tsx", ".json"],
            },
        },
    },

    parserOptions: {
        project: "./tsconfig.json",
        tsconfigRootDir: __dirname,
    },
}
