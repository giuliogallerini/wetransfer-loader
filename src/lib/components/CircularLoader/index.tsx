import { FC, ReactElement } from 'react';
import PropTypes from 'prop-types';
import { CircularLoaderProps } from './types';

import './styles.css';

const diameter = 200;
const strokeWidth = 10;
const radius = ((diameter / 2) - 2) - (strokeWidth * 2);
const circumference = radius * 2 * Math.PI;

const CircularLoader: FC<CircularLoaderProps> = ({ progress, children }): ReactElement => (
  <div className="circular-progresss-root">
    <div className="circular-progresss-content">
      {children}
    </div>
    <svg
      className="circular-progress"
      height={diameter}
      width={diameter}
    >
      <circle
        className="circular-progress-circle-background"
        strokeWidth={strokeWidth}
        stroke="rgb(232, 235, 237)"
        strokeLinecap="round"
        fill="transparent"
        r={radius}
        cx={(diameter / 2)}
        cy={(diameter / 2)}
      />
      <circle
        className="circular-progress-circle"
        strokeWidth={strokeWidth}
        stroke="rgb(64, 159, 255)"
        strokeLinecap="round"
        fill="transparent"
        r={radius}
        cx={diameter / 2}
        cy={diameter / 2}
        style={{
          strokeDasharray: `${circumference} ${circumference}`,
          strokeDashoffset: circumference - (progress / 100) * circumference,
        }}
      />
    </svg>
  </div>
);

CircularLoader.propTypes = {
  progress: PropTypes.number.isRequired,
  children: PropTypes.element,
};

CircularLoader.defaultProps = {
  children: undefined,
};

export default CircularLoader;
