import { ReactElement } from 'react';

export type CircularLoaderProps = {
  progress: number,
  children?: ReactElement,
};
