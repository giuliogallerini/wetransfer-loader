import { MouseEventHandler } from 'react';

export type ButtonPropsType = {
  onClick: MouseEventHandler,
  children: string,
};
