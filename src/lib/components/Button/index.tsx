import { FC } from 'react';
import PropTypes from 'prop-types';

import { ButtonPropsType } from './types';
import './styles.css';

const Button: FC<ButtonPropsType> = ({ onClick, children }) => (
  <button type="button" onClick={onClick}>{children}</button>
);

Button.propTypes = {
  onClick: PropTypes.func.isRequired,
  children: PropTypes.string.isRequired,
};

export default Button;
