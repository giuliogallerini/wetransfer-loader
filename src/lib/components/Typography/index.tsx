import { FC } from 'react';
import PropTypes from 'prop-types';
import { Aligns, TypographyProps, Variants } from './types';

import './styles.css';

const Typography: FC<TypographyProps> = ({ children, variant: Variant, align }) => (
  <Variant className={align}>
    {children}
  </Variant>
);

Typography.propTypes = {
  children: PropTypes.oneOfType([
    PropTypes.element,
    PropTypes.string,
  ]).isRequired,
  variant: PropTypes.oneOf<Variants>(['h1', 'h2', 'h3', 'p']).isRequired,
  align: PropTypes.oneOf<Aligns>(['center', 'left']).isRequired,
};

export default Typography;
