import { ReactElement } from 'react';

export type Variants = 'h1' | 'h2' | 'h3' | 'p';
export type Aligns = 'center' | 'left';

export type TypographyProps = {
  children: ReactElement | string,
  variant: Variants,
  align: Aligns,
};
