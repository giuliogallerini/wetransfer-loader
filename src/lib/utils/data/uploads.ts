import { Recipients, Upload } from 'reducers/uploads/types';

export const recipients: Recipients[] = [
  { email: 'test@domain.com' },
  { email: 'test2@domain.com' },
  { email: 'test3@domain.com' },
  { email: 'test4@domain.com' },
];

export const createUploads = (): Upload[] => [
  {
    id: 1,
    fileSize: 100000,
    progress: 0,
  },
  {
    id: 2,
    fileSize: 2445555,
    progress: 0,
  },
  {
    id: 3,
    fileSize: 4791110,
    progress: 0,
  },
  {
    id: 4,
    fileSize: 7136665,
    progress: 0,
  },
];
