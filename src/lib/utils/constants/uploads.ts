export enum uploadStatuses {
  idle = 1,
  loading = 2,
  stopped = 3,
  done = 4,
}
