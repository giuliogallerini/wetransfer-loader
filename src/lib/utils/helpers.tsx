import { Store } from 'redux';
import { Provider } from 'react-redux'
import { mount } from 'enzyme';
import { ReactElement } from 'react';

export const createMountWithStore = (store: Store) => (component: ReactElement): ReactElement =>
  mount(
    <Provider store={store}>
      {component}
    </Provider>,
  );
