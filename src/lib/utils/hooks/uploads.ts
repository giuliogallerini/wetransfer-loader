import random from 'lodash/random';
import { useEffect, useRef } from 'react';

import { Upload, UploadStatuses } from 'reducers/uploads/types';

import { uploadStatuses } from 'lib/utils/constants/uploads';
import { setProgress } from 'reducers/uploads/actions';
import { useAppDispatch } from './redux';

const interval = 250;
const step = 10;

export const useMockTransfer = (uploads: Upload[], status: UploadStatuses): void => {
  const intervalId: any = useRef();
  const localUploads = [...uploads];
  const dispatch = useAppDispatch();

  useEffect(() => {
    if (status === uploadStatuses.loading && !intervalId.current) {
      const id = setInterval(() => {
        const index = random(0, uploads.length - 1);
        const upload = localUploads[index];

        localUploads[index].progress += step;
        dispatch(setProgress(upload.id, upload.progress + step));
      }, interval);

      intervalId.current = id;
    }

    if (intervalId.current
        && (status === uploadStatuses.stopped || status === uploadStatuses.done)) {
      clearInterval(intervalId.current);
      intervalId.current = undefined;
    }
  }, [uploads, status, intervalId, dispatch, setProgress]);
};
