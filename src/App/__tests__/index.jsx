import { act } from 'react-dom/test-utils';
import { createMountWithStore } from 'lib/utils/helpers';
import { uploadStatuses } from 'lib/utils/constants/uploads';
import store from 'reducers';

import { resumeUpload, startUpload, stopUpload } from 'reducers/uploads/actions';

import {
  selectComplexiveProgress, selectUploadsStatus,
} from 'reducers/uploads/selectors';

import Button from 'lib/components/Button';
import Typography from 'lib/components/Typography';

import App from '..';

jest.mock('reducers/uploads/selectors', () => ({
  ...jest.requireActual('reducers/uploads/selectors'),
  selectUploadsStatus: jest.fn(),
  selectComplexiveProgress: jest.fn(),
}));
jest.mock('reducers/uploads/actions', () => ({
  ...jest.requireActual('reducers/uploads/actions'),
  resumeUpload: jest.fn(),
  startUpload: jest.fn(),
  stopUpload: jest.fn(),
}));

const buttonTexts = {
  [uploadStatuses.idle]: 'Start',
  [uploadStatuses.done]: 'Restart',
  [uploadStatuses.loading]: 'Stop',
  [uploadStatuses.stopped]: 'Resume',
};

const buttonClickHandlers = {
  [uploadStatuses.idle]: startUpload,
  [uploadStatuses.done]: startUpload,
  [uploadStatuses.loading]: stopUpload,
  [uploadStatuses.stopped]: resumeUpload,
};

const titleTexts = {
  [uploadStatuses.idle]: '',
  [uploadStatuses.done]: 'Done 🎉',
  [uploadStatuses.loading]: 'Transfering...',
  [uploadStatuses.stopped]: 'Resume this transfer?',
};

const statuses = [
  'idle',
  'done',
  'loading',
  'stopped',
];

describe('App', () => {
  let mount;

  beforeAll(() => {
    mount = createMountWithStore(store);
  });

  it.each(statuses)('Should pass the %s text to the button', (key) => {
    const status = uploadStatuses[key];
    selectUploadsStatus.mockReturnValue(status);
    selectComplexiveProgress.mockReturnValue(0);

    const component = mount(<App />);
    const button = component.find(Button);

    expect(button.props().children).toEqual(buttonTexts[status]);
  });

  it.each(statuses)('Should pass the %s text to the Typography title', (key) => {
    const status = uploadStatuses[key];
    selectUploadsStatus.mockReturnValue(status);
    selectComplexiveProgress.mockReturnValue(30);
    const text = titleTexts[status];

    const component = mount(<App />);
    const title = component.find(Typography).findWhere((typography) => typography.props().variant === 'h3');

    expect(title.props().children).toEqual(text);
  });

  it.each(statuses)('Should fire the %s click handler when the button is clicked', (key) => {
    const status = uploadStatuses[key];
    selectUploadsStatus.mockReturnValue(status);
    selectComplexiveProgress.mockReturnValue(0);

    const handler = buttonClickHandlers[status];
    handler.mockReturnValue({ type: 'mock' });
    const component = mount(<App />);
    const button = component.find(Button);

    act(() => {
      button.simulate('click');
    });

    expect(handler).toHaveBeenCalledTimes(1);
  });
});
