import { ReactElement, useMemo } from 'react';

import CircularLoader from 'lib/components/CircularLoader';
import Typography from 'lib/components/Typography';
import Button from 'lib/components/Button';

import { resumeUpload, startUpload, stopUpload } from 'reducers/uploads/actions';
import {
  selectComplexiveFileSizeTransfered, selectComplexiveProgress, selectRecipients,
  selectTotalFileSize, selectUploads, selectUploadsStatus,
} from 'reducers/uploads/selectors';

import { uploadStatuses } from 'lib/utils/constants/uploads';
import { useMockTransfer } from 'lib/utils/hooks/uploads';
import { useAppDispatch, useAppSelector } from 'lib/utils/hooks/redux';
import { recipients, createUploads } from 'lib/utils/data/uploads';

import './styles.css';

function App(): ReactElement {
  // data selected from the store
  const dispact = useAppDispatch();
  const progressStatus = useAppSelector(selectUploadsStatus);
  const uploads = useAppSelector(selectUploads);
  const nPeople = useAppSelector(selectRecipients).length;
  const progress = useAppSelector(selectComplexiveProgress);
  const dataTransfered = useAppSelector(selectComplexiveFileSizeTransfered) / 1000000; // in GB
  const transferSize = useAppSelector(selectTotalFileSize) / 1000000; // in GB
  // dispatch actions
  const handleStartUpload = () => {
    const initalUploads = createUploads();
    dispact(startUpload(initalUploads, recipients));
  };
  const handleStopUpload = () => dispact(stopUpload());
  const handleResumeUpload = () => dispact(resumeUpload());
  // event handlers
  const handleClick = useMemo(() => {
    switch (progressStatus) {
      case uploadStatuses.idle:
      case uploadStatuses.done:
        return handleStartUpload;
      case uploadStatuses.loading: return handleStopUpload;
      case uploadStatuses.stopped: return handleResumeUpload;

      default: return handleStartUpload;
    }
  }, [progressStatus]);
  // dynamic text
  const buttonText = useMemo(() => {
    switch (progressStatus) {
      case uploadStatuses.idle: return 'Start';
      case uploadStatuses.done: return 'Restart';
      case uploadStatuses.loading: return 'Stop';
      case uploadStatuses.stopped: return 'Resume';

      default: return 'Start';
    }
  }, [progressStatus]);
  const title = useMemo(() => {
    switch (progressStatus) {
      case uploadStatuses.done: return 'Done 🎉';
      case uploadStatuses.loading: return 'Transfering...';
      case uploadStatuses.stopped: return 'Resume this transfer?';

      case uploadStatuses.idle:
      default: return '';
    }
  }, [progressStatus]);
  const description: string = useMemo(() => {
    switch (progressStatus) {
      case uploadStatuses.done: return 'Your files have been transferred';
      case uploadStatuses.loading:
      case uploadStatuses.stopped: return `Sending ${uploads.length} files to ${nPeople} recipients
      ${dataTransfered.toFixed(2)}GB of ${transferSize.toFixed(2)}GB uploaded`;

      case uploadStatuses.idle:
      default: return '';
    }
  }, [uploads, nPeople, dataTransfered, transferSize, progressStatus]);

  useMockTransfer(uploads, progressStatus);

  return (
    <div className="root">
      <div className="upload-card">
        <CircularLoader progress={progress}>
          <Typography align="center" variant="h1">
            <>
              {`${progress}`}
              <span>%</span>
            </>
          </Typography>
        </CircularLoader>
        <Typography variant="h3" align="center">{title}</Typography>
        <Typography variant="p" align="center">{description}</Typography>
        <div className="actions">
          <Button onClick={handleClick}>{buttonText}</Button>
        </div>
      </div>
    </div>
  );
}

export default App;
