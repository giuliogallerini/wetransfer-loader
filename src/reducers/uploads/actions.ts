import {
  RESUME_UPLOAD, SET_PROGRESS, START_UPLOAD, STOP_UPLOAD,
} from './actionNames';
import { Recipients, Upload, UploadAction } from './types';

export const startUpload = (uploads: Upload[], recipients: Recipients[]): UploadAction => ({
  type: START_UPLOAD,
  payload: {
    uploads,
    recipients,
  },
});

export const stopUpload = (): UploadAction => ({
  type: STOP_UPLOAD,
});

export const resumeUpload = (): UploadAction => ({
  type: RESUME_UPLOAD,
});

export const setProgress = (uploadId: number, progress: number): UploadAction => ({
  type: SET_PROGRESS,
  payload: {
    uploadId,
    progress,
  },
});
