export const reducerName = 'uploads' as const;

export const START_UPLOAD = `${reducerName}.start` as const;
export const STOP_UPLOAD = `${reducerName}.stop` as const;
export const RESUME_UPLOAD = `${reducerName}.resume` as const;
export const SET_PROGRESS = `${reducerName}.updateProgress` as const;
