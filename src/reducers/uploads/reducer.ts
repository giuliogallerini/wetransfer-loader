import every from 'lodash/every';

import { uploadStatuses } from 'lib/utils/constants/uploads';
import {
  RESUME_UPLOAD, SET_PROGRESS, START_UPLOAD, STOP_UPLOAD,
} from './actionNames';
import { UploadAction, UploadsState } from './types';

export const initialState: UploadsState = {
  uploads: [],
  recipients: [],
  status: uploadStatuses.idle,
};

export default function uploadsReducer(state?: UploadsState, action?: UploadAction): UploadsState {
  switch (action?.type) {
    case START_UPLOAD: {
      const { uploads, recipients } = action?.payload || {};

      if (!uploads?.length || !recipients?.length) return state || initialState;

      return {
        ...(state || initialState),
        uploads,
        recipients,
        status: uploadStatuses.loading,
      };
    }

    case STOP_UPLOAD:
      return {
        ...(state || initialState),
        status: uploadStatuses.stopped,
      };

    case RESUME_UPLOAD:
      return {
        ...(state || initialState),
        status: uploadStatuses.loading,
      };

    case SET_PROGRESS: {
      const { uploadId, progress } = action?.payload || {};

      if (!uploadId || typeof progress !== 'number') return state || initialState;

      const uploads = state?.uploads?.map(
        (upload) => upload.id === uploadId
          ? { ...upload, progress: progress > 100 ? 100 : progress }
          : upload,
      ) || [];

      const status = every(uploads, ({ progress: uploadProgress }) => uploadProgress === 100)
        ? uploadStatuses.done
        : (state?.status || uploadStatuses.loading);

      return {
        ...(state || initialState),
        uploads,
        status,
      };
    }

    default: return state || initialState;
  }
}
