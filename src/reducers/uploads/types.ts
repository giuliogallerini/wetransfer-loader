import { Action, StoreEnhancer } from 'redux';

export type UploadStatuses = 1 | 2 | 3 | 4;

export type Upload = {
  id: number,
  progress: number,
  fileSize: number,
};

export type Recipients = {
  email: string,
};

export type UploadsState = {
  uploads: Upload[],
  recipients: Recipients[],
  status: UploadStatuses,
};

export interface UploadAction extends Action {
  payload?: {
    recipients?: Recipients[],
    uploads?: Upload[],
    uploadId?: number,
    progress?: number,
  }
}

declare global {
  interface Window {
    __REDUX_DEVTOOLS_EXTENSION_COMPOSE__: () => StoreEnhancer
  }
}
