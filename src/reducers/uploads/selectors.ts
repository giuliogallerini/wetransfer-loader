import { uploadStatuses } from 'lib/utils/constants/uploads';
import { createSelector } from 'reselect';

import {
  Recipients, Upload, UploadsState, UploadStatuses,
} from './types';

export const selectUploadsStatus = (state: UploadsState): UploadStatuses =>
  state?.status || uploadStatuses.idle;

export const selectUploads = (state: UploadsState): Upload[] => state?.uploads;
export const selectRecipients = (state: UploadsState): Recipients[] => state?.recipients;

export const selectTotalFileSize = createSelector(
  selectUploads,
  (uploads) => uploads.reduce((sum, { fileSize }) => sum + fileSize, 0),
);

export const selectComplexiveProgress = createSelector(
  selectUploads,
  (uploads) => uploads?.length
    ? Math.round(uploads.reduce((sum, { progress }) => sum + progress, 0) / uploads.length)
    : 0,
);

export const selectComplexiveFileSizeTransfered = createSelector(
  selectTotalFileSize,
  selectComplexiveProgress,
  (transferSize, progress) => transferSize ? (transferSize * progress) / 100 : 0,
);
