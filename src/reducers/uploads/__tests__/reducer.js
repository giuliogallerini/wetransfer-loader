import { uploadStatuses } from 'lib/utils/constants/uploads';
import {
  RESUME_UPLOAD, SET_PROGRESS, START_UPLOAD, STOP_UPLOAD,
} from '../actionNames';
import reducer, { initialState } from '../reducer';

describe('upload reducer', () => {
  describe('START_UPLOAD', () => {
    it('Should return initial state if no uploads are passed', () => {
      const recipients = [{ email: 'test@domain.com' }];
      const action = {
        type: START_UPLOAD,
        payload: {
          recipients,
        },
      };

      const state = reducer(initialState, action);

      expect(state).toEqual(initialState);
    });

    it('Should return initial state if no recipients are passed', () => {
      const uploads = [{ id: 1, fileSize: 1000, progress: 0 }];
      const action = {
        type: START_UPLOAD,
        payload: {
          uploads,
        },
      };

      const state = reducer(initialState, action);

      expect(state).toEqual(initialState);
    });

    it('Should set uploads and receipients and update the status to loading', () => {
      const uploads = [{ id: 1, fileSize: 1000, progress: 0 }];
      const recipients = [{ email: 'test@domain.com' }];
      const action = {
        type: START_UPLOAD,
        payload: {
          uploads,
          recipients,
        },
      };

      const state = reducer(initialState, action);

      expect(state).toEqual({
        ...initialState,
        uploads,
        recipients,
        status: uploadStatuses.loading,
      });
    });
  });

  describe('STOP_UPLOAD', () => {
    it('Should set the status to stopped', () => {
      const action = {
        type: STOP_UPLOAD,
      };

      const state = reducer(initialState, action);

      expect(state).toEqual({
        ...initialState,
        status: uploadStatuses.stopped,
      });
    });
  });

  describe('RESUME_UPLOAD', () => {
    it('Should set the status to stopped', () => {
      const action = {
        type: RESUME_UPLOAD,
      };

      const state = reducer(initialState, action);

      expect(state).toEqual({
        ...initialState,
        status: uploadStatuses.loading,
      });
    });
  });

  describe('SET_PROGRESS', () => {
    it('Should return intial state if uploadId is not passed', () => {
      const action = {
        type: SET_PROGRESS,
        payload: {
          progress: 100,
        },
      };

      const state = reducer(initialState, action);

      expect(state).toEqual(initialState);
    });

    it('Should return intial state if progress is not passed', () => {
      const action = {
        type: SET_PROGRESS,
        payload: {
          uploadId: 1,
        },
      };

      const state = reducer(initialState, action);

      expect(state).toEqual(initialState);
    });

    it('Should return intial state if progress is not a number', () => {
      const action = {
        type: SET_PROGRESS,
        payload: {
          uploadId: 1,
          progress: '10',
        },
      };

      const state = reducer(initialState, action);

      expect(state).toEqual(initialState);
    });

    it('Should update the upload progress', () => {
      const action = {
        type: SET_PROGRESS,
        payload: {
          uploadId: 1,
          progress: 10,
        },
      };

      let state = {
        ...initialState,
        uploads: [{ id: 1, progress: 0, fileSize: 100 }],
      };

      state = reducer(state, action);

      expect(state).toEqual({
        ...initialState,
        uploads: [{ id: 1, progress: 10, fileSize: 100 }],
      });
    });

    it('Should max the upload progress to 100', () => {
      const action = {
        type: SET_PROGRESS,
        payload: {
          uploadId: 1,
          progress: 101,
        },
      };

      let state = {
        ...initialState,
        uploads: [{ id: 1, progress: 0, fileSize: 100 }, { id: 2, progress: 0, fileSize: 100 }],
      };

      state = reducer(state, action);

      expect(state).toEqual({
        ...initialState,
        uploads: [{ id: 1, progress: 100, fileSize: 100 }, { id: 2, progress: 0, fileSize: 100 }],
      });
    });

    it('Should set status to done if all uploads progresses reach 100', () => {
      const action = {
        type: SET_PROGRESS,
        payload: {
          uploadId: 1,
          progress: 100,
        },
      };

      let state = {
        ...initialState,
        uploads: [{ id: 1, progress: 0, fileSize: 100 }, { id: 2, progress: 100, fileSize: 100 }],
      };

      state = reducer(state, action);

      expect(state).toEqual({
        ...initialState,
        uploads: [{ id: 1, progress: 100, fileSize: 100 }, { id: 2, progress: 100, fileSize: 100 }],
        status: uploadStatuses.done,
      });
    });
  });
});
