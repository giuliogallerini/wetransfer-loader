import { initialState } from '../reducer';
import { selectComplexiveFileSizeTransfered, selectComplexiveProgress, selectTotalFileSize } from '../selectors';

describe('uploads selectors', () => {
  describe('selectTotalFileSize', () => {
    it('Should return the sum of all the file sizes', () => {
      const state = {
        ...initialState,
        uploads: [{ id: 1, fileSize: 1, progress: 0 }, { id: 1, fileSize: 10, progress: 0 }],
      };

      const totalFileSize = selectTotalFileSize(state);

      expect(totalFileSize).toEqual(11);
    });
  });

  describe('selectComplexiveProgress', () => {
    it('Should return 0 if there are no uploads', () => {
      const complexiveProgress = selectComplexiveProgress(initialState);

      expect(complexiveProgress).toEqual(0);
    });

    it('Should return the sum of all the file sizes', () => {
      const state = {
        ...initialState,
        uploads: [{ id: 1, fileSize: 1, progress: 10 }, { id: 1, fileSize: 10, progress: 20 }],
      };

      const complexiveProgress = selectComplexiveProgress(state);

      expect(complexiveProgress).toEqual(15);
    });
  });

  describe('selectComplexiveFileSizeTransfered', () => {
    it('Should return 0 if there are no uploads', () => {
      const kbTransferred = selectComplexiveFileSizeTransfered(initialState);

      expect(kbTransferred).toEqual(0);
    });

    it('Should return the complexive data transferred currently', () => {
      const state = {
        ...initialState,
        uploads: [{ id: 1, fileSize: 100, progress: 10 }, { id: 1, fileSize: 100, progress: 20 }],
      };

      const kbTransferred = selectComplexiveFileSizeTransfered(state);

      expect(kbTransferred).toEqual(30);
    });
  });
});
