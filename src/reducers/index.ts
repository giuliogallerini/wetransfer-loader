import { compose as extCompose, createStore } from 'redux';

import uploadsReducer, { initialState } from './uploads/reducer';

const compose = window.__REDUX_DEVTOOLS_EXTENSION_COMPOSE__ || extCompose;
const store = createStore(uploadsReducer, initialState, compose());

export type RootState = ReturnType<typeof store.getState>;
export type AppDispatch = typeof store.dispatch;

export default store;
