# [WeTransfer Loader](http://wetransfer.giuliogallerini.com)

## Tech used

For this assignment, I used create-react-app to generate the app using the js preset.
Once started the project I realized that, since WeTranfer products are heavily using TypeScript, you would probably be interested in knowing my level in the language (also TypeScript is awesome :smile:).

The assignment states quite clearly that WeTransfer has huge React/Redux foundations, that's why I decided to use them in the building of this small application, even if the same result could have been achieved in a much easier way.

As testing frameworks, I have used Jest + Enzyme. I found Enzyme structure and syntax way for understandable than `@testing-library`, that's the reason behind this choice, and Jest... well I don't think it has any rival :lol:

I have used `reselect` for memoization of selected state-based and `lodash` for some minor collection utilities.

The application might seem complex (not really, however it might be for an assignment) but I just wanted to touch all the points that I thought were relevant for the position, and for you to get an understanding of my skills.

## Application Breakdown

I have structured my application in three main folders:

- App: where the actual application logic begins and reusable components are implemented together to form screens
- lib: Library of
  - components: Reusable components
  - ultis: Utilities
    - constants: Constants to be reused throughout the application
    - data: Mock data
    - hook: Reusable Hooks, in this case, there's just one real custom hook (`useMockTransfer`) that I have custom-created to replicate the transfer behavior for the loader. The other hooks are provided by the `react-redux` docs.
- reducers: The collection of data management reducers on the app, in this specific case there's only one...
  - uploads: The whole logic of the application is contained here, and it's subdivided in...
    - actionNames: Constants representing the possible action types for the uploads reducer
    - action: Definition of plain actions that simply pass the data from the components to the reducer
    - reducer: Where the actual logic begins and the state is updated
    - selectors: Here I manipulate the data for visual representation in the components.
    - type: Type definition for the uploads reducer

This is a brief breakdown of the application structure, in the next section, I will go more into details on the `components` structure, as requested in the assignment.

## Reusable Components

I have separated them from the actual application because in the years I have learned that having a clear separation between elements that have different purposes is key for maintainability and scalability.

The structure of a reusable component is quite simple:

- Component/
  - index.tsx: where the component is defined and exported
  - styles.css: plain css styles that are then imported into the index.tsx
  - types.ts: type definitions for the component

As you can see, also within the component itself I like to keep a clear separation based on the code use case.
In this way when another developer or, even myself in few months, needs to make changes in the code it is immediately clear where everything is, without even needing to open the files.

## Application

I declared most of the visual logic here since the application is quite simple and I wanted to keep the `lib/components` as reusable as possible.

## Conclusion

I had a lot of fun doing this assignment, and I even published it under my domain [http://wetransfer.giuliogallerini.com](http://wetransfer.giuliogallerini.com), go check it out and play with it if you want :tada:

P.S.: You can also open it on your Phone :wink:
